package nl.bioinf.ccpeters.primer;


/**
 * This class is for holding onto a molecular identity match, it saves the match itself and the size.
 * There's also a handy function for getting the reverse translated match, so it can be looked up more easily
 * on the webpage.
 */

public class MolecularIdentityMatch {
    private int maxIdentityMatchSize;
    private String foundMatch;

    MolecularIdentityMatch(String foundMatch, int maxIdentityMatchSize) {
        this.maxIdentityMatchSize = maxIdentityMatchSize;
        this.foundMatch = foundMatch;
    }

    public int getMaxIdentityMatchSize() {
        return maxIdentityMatchSize;
    }

    public String getFoundMatch() {
        return foundMatch;
    }

    public String getFoundReverseTranslatedMatch() {
        StringBuilder reverseTranslated = new StringBuilder();
        for (int i = this.foundMatch.length() - 1; i >= 0; i--) {
            char currentNucleotide = this.foundMatch.charAt(i);
            if (currentNucleotide == 'A' || currentNucleotide == 'a') {
                currentNucleotide = 'T';
            }
            else if (currentNucleotide == 'T' || currentNucleotide == 't') {
                currentNucleotide = 'A';
            }
            else if (currentNucleotide == 'G' || currentNucleotide == 'g') {
                currentNucleotide = 'C';
            }
            else if (currentNucleotide == 'C' || currentNucleotide == 'c') {
                currentNucleotide = 'G';
            }
            reverseTranslated.append(currentNucleotide);
        }
        return reverseTranslated.toString();
    }
}
