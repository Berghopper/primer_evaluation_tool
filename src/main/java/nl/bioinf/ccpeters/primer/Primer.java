package nl.bioinf.ccpeters.primer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class primer, this object holds the primer DNA sequence and will automatically calculate some things, like gc
 * percentage, melting temp and intramolecular identity. Also input gets checked before anything is calculated.
 */

public class Primer {

    private String primer;
    private double gcPercentage;
    private double meltingTemperature;
    private MolecularIdentityMatch maxIntraMolecularIdentity;
    private boolean isCorrect = true;


    public Primer(String primer) {
        this.primer = primer;
        this.isCorrect = hasLegalDNA();
        if (isCorrect) {
            this.gcPercentage = calculateGC();
            this.meltingTemperature = calculateMeltingTemperature();
            this.maxIntraMolecularIdentity = calculateMaxIntraMolecularIdentity();
        }
    }

    private boolean hasLegalDNA(){
        Pattern p = Pattern.compile("^[ACGTacgt]+$");
        Matcher m = p.matcher(this.primer);
        return m.matches();
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public String getPrimer() {
        return primer;
    }

    private int countPatternOccurence(String thePattern) {
        // Count the pattern occurence in the primer, useful for determining the amount of GC or indepent bases.
        Pattern pattern = Pattern.compile(thePattern);
        Matcher matcher = pattern.matcher(this.primer);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    private double calculateGC() {
        int count = countPatternOccurence("[GgCc]");
        return (((double) count)/((double) this.primer.length()))*100.00;
    }

    private double calculateMeltingTemperature(){
        // Depending on how long the primer is, the formula might differ.
        double meltingTemperature;
        // first count occurences of all nucleotides as this is neccesary for all the formulas.
        int AdenineCount = countPatternOccurence("[Aa]");
        int CytosineCount = countPatternOccurence("[Cc]");
        int ThymineCount = countPatternOccurence("[Tt]");
        int GuanineCount = countPatternOccurence("[Gg]");
        if (this.primer.length() < 14) {
            // for less than 14 nucleotides the formula is; Tm= (A+T) * 2 + (G+C) * 4
            meltingTemperature = ((double) (AdenineCount+ThymineCount)) * 2 + ((double) (GuanineCount+CytosineCount))
                    * 4;
        }
        else {
            // for sequences longer than 13 nucleotides, the equation used is; Tm= 64.9 +41*(G+C-16.4)/(A+T+G+C)
            meltingTemperature = (64.9 + 41.0 * (((double) (GuanineCount+CytosineCount))-16.4)/((double)
                    (AdenineCount+ThymineCount+GuanineCount+CytosineCount)));
        }
        return meltingTemperature;
    }

    private ArrayList<String> calculateAllHomopolymerStretches(String sequence, String nucleotide) {
        sequence = sequence.toUpperCase();
        ArrayList<String> allHomopolymerStretches = new ArrayList<>();
        for (int i = 1; i <= sequence.length(); i++) {
            String stretch = String.join("", Collections.nCopies(i, nucleotide));
            Pattern pattern = Pattern.compile(stretch);
            Matcher matcher = pattern.matcher(sequence);
            if (matcher.find()) {
                allHomopolymerStretches.add(stretch);
            }
        }
        return  allHomopolymerStretches;
    }

    public ArrayList<String> calculateMaxHomopolymerStretches(Primer otherprimer) {
        ArrayList<String> maxHomopolymerStretches = new ArrayList<>();
        // loop through each sequence seperately first and get ALL stretches
        ArrayList<String> primerBStretches = new ArrayList<>();
        primerBStretches.addAll(calculateAllHomopolymerStretches(otherprimer.getPrimer(), "A"));
        primerBStretches.addAll(calculateAllHomopolymerStretches(otherprimer.getPrimer(), "G"));
        primerBStretches.addAll(calculateAllHomopolymerStretches(otherprimer.getPrimer(), "C"));
        primerBStretches.addAll(calculateAllHomopolymerStretches(otherprimer.getPrimer(), "T"));
        ArrayList<String> primerAStretches = new ArrayList<>();
        primerAStretches.addAll(calculateAllHomopolymerStretches(this.getPrimer(), "A"));
        primerAStretches.addAll(calculateAllHomopolymerStretches(this.getPrimer(), "G"));
        primerAStretches.addAll(calculateAllHomopolymerStretches(this.getPrimer(), "C"));
        primerAStretches.addAll(calculateAllHomopolymerStretches(this.getPrimer(), "T"));

        // Make one list with matching sequences
        ArrayList<String> combinedStretches = new ArrayList<>();
        for (String primerAStretch : primerAStretches) {
            for (String primerBStretch : primerBStretches) {
                if (primerAStretch.toUpperCase().equals(primerBStretch.toUpperCase())) {
                    combinedStretches.add(primerAStretch.toUpperCase());
                }
            }
        }
        // grab the biggest one
        // first determine what the biggest lengths are
        int biggestStringLength = 0;
        for (String stretch : combinedStretches) {
            if (stretch.length() > biggestStringLength) {
                biggestStringLength = stretch.length();
            }
        }
        // remove all items that are smaller so you are left with the biggest ones.
        for (String stretch : combinedStretches) {
            if (stretch.length() == biggestStringLength) {
                maxHomopolymerStretches.add(stretch);
            }
        }
        return maxHomopolymerStretches;
    }


    private MolecularIdentityMatch calculateMaxMolecularIdentity(Primer otherprimer) {
        MolecularIdentityMatch molecularIdentityMatch = null;
        // Get the reverse translated primer to be able to match exactly.
        String otherReversePrimer = otherprimer.getReverseTranslatedPrimer();
        // Base Identity finding on the smallest primer, this prevents from a too big string trying to be fit into a
        // smaller primer / string
        String bigPrimer;
        String smallPrimer;
        if (this.primer.length() > otherReversePrimer.length()) {
            bigPrimer = this.primer;
            smallPrimer = otherReversePrimer;
        }
        else if (this.primer.length() < otherReversePrimer.length()) {
            bigPrimer = otherReversePrimer;
            smallPrimer = this.primer;
        }
        else {
            bigPrimer = this.primer;
            smallPrimer = otherReversePrimer;
        }
        // generate all possible substring lengths, beginning from the largest possible, stop once an identity string
        // has been found.
        boolean foundNothing = true;
        for (int substringLength = smallPrimer.length(); substringLength >= 0; substringLength--) {
            // construct all possible substrings out of current length, if multiple are possible.
            if (foundNothing) {
                for (int startindex = 0, stopindex = substringLength; stopindex <= smallPrimer.length(); startindex++,
                        stopindex++) {
                    String mySubstring = smallPrimer.substring(startindex, stopindex);
                    // do not continue if there's nothing to be found.
                    if (!(mySubstring.isEmpty())) {
                        if (bigPrimer.toLowerCase().contains(mySubstring.toLowerCase())) {
                            // if a match is found, this is automatically the biggest, break out of the for loop.
                            molecularIdentityMatch = new MolecularIdentityMatch(mySubstring, mySubstring.length());
                            foundNothing = false;
                            break;
                        }
                    }
                }
            }
            else {
                break;
            }
        }
        return molecularIdentityMatch;
    }


    public MolecularIdentityMatch calculateMaxInterMolecularIdentity(Primer otherprimer) {
        return calculateMaxMolecularIdentity(otherprimer);
    }


    private MolecularIdentityMatch calculateMaxIntraMolecularIdentity() {
        return calculateMaxMolecularIdentity(this);
    }


    private String getReverseTranslatedPrimer() {
        StringBuilder reverseTranslated = new StringBuilder();
        for (int i = this.primer.length() - 1 ; i >= 0 ; i--) {
            char currentNucleotide = this.primer.charAt(i);
            if (currentNucleotide == 'A' || currentNucleotide == 'a') {
                currentNucleotide = 'T';
            }
            else if (currentNucleotide == 'T' || currentNucleotide == 't') {
                currentNucleotide = 'A';
            }
            else if (currentNucleotide == 'G' || currentNucleotide == 'g') {
                currentNucleotide = 'C';
            }
            else if (currentNucleotide == 'C' || currentNucleotide == 'c') {
                currentNucleotide = 'G';
            }
            reverseTranslated.append(currentNucleotide);
        }
        return reverseTranslated.toString();
    }


    public double getGcPercentage() {
        return gcPercentage;
    }


    public double getMeltingTemperature() {
        return meltingTemperature;
    }


    public MolecularIdentityMatch getMaxIntraMolecularIdentity() {
        return maxIntraMolecularIdentity;
    }
}
