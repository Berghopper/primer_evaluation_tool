package nl.bioinf.ccpeters.servlets;

import nl.bioinf.ccpeters.primer.MolecularIdentityMatch;
import nl.bioinf.ccpeters.primer.Primer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This is the PrimerServlet that will serve out the primer webpage and handle views / inputs.
 */

@WebServlet(name = "PrimerServlet", urlPatterns = "/primer")
public class PrimerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        request.setAttribute("submitted", "true");
        String primerAParam = request.getParameter("primerA");
        String primerBParam = request.getParameter("primerB");
        if (primerAParam.isEmpty() && primerBParam.isEmpty()) {
            request.setAttribute("wrongParameters", "Incorrect input given, at least one ore two fields need " +
                    "to be input.");
            request.setAttribute("submitted", "");
        }
        else if (primerAParam.isEmpty()) {
            Primer singlePrimer = new Primer(primerBParam);
            request = calculateOneInput(request, singlePrimer);
        }
        else if (primerBParam.isEmpty()) {
            Primer singlePrimer = new Primer(primerAParam);
            request = calculateOneInput(request, singlePrimer);
        }
        else {
            Primer primerA = new Primer(primerAParam);
            Primer primerB = new Primer(primerBParam);
            request = calculateTwoInputs(request, primerA, primerB);
        }
        RequestDispatcher view = request.getRequestDispatcher("primer.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        // return view (webpage) if form is not submitted.
        RequestDispatcher view = request.getRequestDispatcher("primer.jsp");
        view.forward(request, response);
    }

    private HttpServletRequest calculateTwoInputs(HttpServletRequest request, Primer primerA, Primer primerB) {
        if (primerA.isCorrect() && primerB.isCorrect()) {
            request.setAttribute("primerA", "Primer A is: " + primerA.getPrimer());
            request.setAttribute("primerB", "Primer B is: " + primerB.getPrimer());
            request.setAttribute("primerAGC", String.format("Primer A GC-percentage: %.2f",
                    primerA.getGcPercentage())+"%");
            request.setAttribute("primerBGC", String.format("Primer B GC-percentage: %.2f",
                    primerB.getGcPercentage())+"%");
            request.setAttribute("primerAMelt", String.format("Primer A Melting temperature: %.2f °C",
                    primerA.getMeltingTemperature()));
            request.setAttribute("primerBMelt", String.format("Primer B Melting temperature: %.2f °C",
                    primerB.getMeltingTemperature()));
            // get maxHomopolymerStretches
            ArrayList<String> maxHomopolymerStretches = primerA.calculateMaxHomopolymerStretches(primerB);
            if (maxHomopolymerStretches.isEmpty()) {
                request.setAttribute("primersMaxHomopolymerStretch", "No homopolymer stretches were found!");
            }
            else {
                StringBuilder maxHomopolymerStretchesRequestString = new StringBuilder();
                maxHomopolymerStretchesRequestString.append("The following String(s) are the largest homopolymer " +
                        "stretches: ");
                for (String maxHomopolymerStretch : maxHomopolymerStretches) {
                    maxHomopolymerStretchesRequestString.append(maxHomopolymerStretch);
                    maxHomopolymerStretchesRequestString.append(", ");
                }
                maxHomopolymerStretchesRequestString.setLength(maxHomopolymerStretchesRequestString.length() - 2);
                maxHomopolymerStretchesRequestString.append(String.format("  | length = %d",
                        maxHomopolymerStretches.get(0).length()
                ));
                request.setAttribute("primersMaxHomopolymerStretch",
                        maxHomopolymerStretchesRequestString.toString());
            }
            // calculate maxInterMolecularIdentity between primers
            MolecularIdentityMatch maxInterMolecularIdentityMatch = primerA.calculateMaxInterMolecularIdentity(primerB);
            if (maxInterMolecularIdentityMatch != null) {
                request.setAttribute("primersMaxInterMolIdentity", String.format("Maximum intermolecular " +
                                "identity match is a size of; %d (found with the following match; %s / %s)",
                        maxInterMolecularIdentityMatch.getMaxIdentityMatchSize(),
                        maxInterMolecularIdentityMatch.getFoundMatch(),
                        maxInterMolecularIdentityMatch.getFoundReverseTranslatedMatch()
                ));
            }
            else {
                request.setAttribute("primersMaxInterMolIdentity", "no intermolecular matches found!");
            }
            // calculate maxIntraMolecularIdentity for primerA
            MolecularIdentityMatch maxIntraMolecularIdentityMatchPrimerA = primerA.getMaxIntraMolecularIdentity();
            if (maxIntraMolecularIdentityMatchPrimerA != null) {
                request.setAttribute("primerAMaxIntraMolIdentity",
                        String.format("Maximum intramolecular identity match of Primer A is a size of; %d " +
                                        "(found with the following match; %s / %s)",
                                maxIntraMolecularIdentityMatchPrimerA.getMaxIdentityMatchSize(),
                                maxIntraMolecularIdentityMatchPrimerA.getFoundMatch(),
                                maxIntraMolecularIdentityMatchPrimerA.getFoundReverseTranslatedMatch()
                        ));
            }
            else {
                request.setAttribute("primerAMaxIntraMolIdentity", "no intramolecular matches found for primer " +
                        "A!");
            }
            // calculate maxIntraMolecularIdentity for primerB
            MolecularIdentityMatch maxIntraMolecularIdentityMatchPrimerB = primerB.getMaxIntraMolecularIdentity();
            if (maxIntraMolecularIdentityMatchPrimerB != null) {
                request.setAttribute("primerBMaxIntraMolIdentity", String.format("Maximum intramolecular " +
                                "identity match of Primer B is a size of; %d (found with the following match; %s / %s)",
                        maxIntraMolecularIdentityMatchPrimerB.getMaxIdentityMatchSize(),
                        maxIntraMolecularIdentityMatchPrimerB.getFoundMatch(),
                        maxIntraMolecularIdentityMatchPrimerB.getFoundReverseTranslatedMatch()
                ));
            }
            else {
                request.setAttribute("primerBMaxIntraMolIdentity", "no intramolecular matches found for primer " +
                        "B!");
            }

        }
        else {
            request.setAttribute("wrongParameters", "Incorrect input given, only correct DNA containing the " +
                    "characters: \"A\". \"C\", \"G\", \"T\", \"a\", \"c\", \"g\" and \"t\" are accepted.");
            request.setAttribute("submitted", "");
        }
        return request;
    }

    private HttpServletRequest calculateOneInput(HttpServletRequest request, Primer singlePrimer) {
        if (singlePrimer.isCorrect()) {
            request.setAttribute("primerA", "Primer is: " + singlePrimer.getPrimer());
            request.setAttribute("primerAGC", String.format("Primer GC-percentage: %.2f",
                    singlePrimer.getGcPercentage())+"%");
            request.setAttribute("primerAMelt", String.format("Primer Melting temperature: %.2f °C",
                    singlePrimer.getMeltingTemperature()));
            // calculate maxIntraMolecularIdentity for primer
            MolecularIdentityMatch maxIntraMolecularIdentityMatch = singlePrimer.getMaxIntraMolecularIdentity();
            if (maxIntraMolecularIdentityMatch != null) {
                request.setAttribute("primerAMaxIntraMolIdentity",
                        String.format("Maximum intramolecular identity match of Primer is a size of; %d " +
                                        "(found with the following match; %s / %s)",
                                maxIntraMolecularIdentityMatch.getMaxIdentityMatchSize(),
                                maxIntraMolecularIdentityMatch.getFoundMatch(),
                                maxIntraMolecularIdentityMatch.getFoundReverseTranslatedMatch()
                        ));
            }
            else {
                request.setAttribute("primerAMaxIntraMolIdentity", "no intramolecular matches found for Primer!");
            }
        }
        else {
            request.setAttribute("wrongParameters", "Incorrect input given, only correct DNA containing the " +
                    "characters: \"A\". \"C\", \"G\", \"T\", \"a\", \"c\", \"g\" and \"t\" are accepted.");
            request.setAttribute("submitted", "");
        }
        return request;
    }
}
