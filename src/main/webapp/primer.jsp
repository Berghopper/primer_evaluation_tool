<%--
  Created by IntelliJ IDEA.
  User: Casper
  Date: 24/12/2017
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Primer Evaluation Tool</title>
    <link rel="stylesheet" href="css/primerStyling.css">
</head>
<body bgcolor="#ffe2b5">
    <h2>Primer Evaluation Tool</h2>
    <p id="help" class="help"></p>
    <div id="helpdiv" class="spoiler">
        <h3>What is this tool?</h3>
        <p>This tool is meant for analysing DNA primer(s) in multiple ways:</p>
        <ul>
            <li>GC percentage</li>
            <li>melting point</li>
            <li>the maximum homopolymer stretch between 2 primers</li>
            <li>the maximum intermolecular identity between 2 primers</li>
            <li>the maximum intramolecular identity of the primer(s)</li>
        </ul>
        <p>
            A homopolymer stretch is a repition of 1 base.
            An intermolecular identity is
            a complement match between the 2 primers, where one of the primers is reverse stranded.
            An intramolecular identity is a complement match within the same primer on its own reverse strand.
        </p>
        <p>
            At least one or both primers need to be input, the only legal DNA characters to use are "A", "C", "T",
            "G", "a", "c", "t" and "g".
        </p>
    </div>
    <p>Input your primers here:</p>
    <form action="primer" method = "post" id="theForm" name=theForm">
        <p>Fill in your first primer (Primer A): </p><input type="text" name="primerA" id="primerA"><br>
        <p>Fill in your second primer (Primer B): </p><input type="text" name="primerB" id="primerB"><br>
        <br>
        <input type = "submit" value = "Submit primers"/>
    </form>
    <p style="color:red;">${requestScope.wrongParameters}</p>
    <div id="fadeInDiv">
        <p id="submitted" hidden>${requestScope.submitted}</p>
        <p>${requestScope.primerA}</p>
        <p>${requestScope.primerB}</p>
        <div id="hrdiv" class="hrdiv">
            <hr>
        </div>
        <p>${requestScope.primerAGC}</p>
        <p>${requestScope.primerBGC}</p>
        <p>${requestScope.primerAMelt}</p>
        <p>${requestScope.primerBMelt}</p>
        <p>${requestScope.primersMaxHomopolymerStretch}</p>
        <p>${requestScope.primersMaxInterMolIdentity}</p>
        <p>${requestScope.primerAMaxIntraMolIdentity}</p>
        <p>${requestScope.primerBMaxIntraMolIdentity}</p>
    </div>
    <script src="js/UI.js"></script>
</body>
</html>
