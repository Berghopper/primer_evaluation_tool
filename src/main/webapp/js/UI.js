// set some variables to either show / hide things, this is not in the html to make the page still usable if
// js is not supported / blocked.

fadeInDiv = document.getElementById("fadeInDiv");
fadeInDiv.style.opacity = 0;
fadeInDiv.style.display = "none";
helpDiv = document.getElementById("helpdiv");
helpDiv.style.opacity = 0;
helpDiv.style.display = "none";
document.getElementById("help").innerHTML = "Show help <button class=\"spoilerbutton\" onclick=\"fadeInHelp()" +
    "\">+</button>";


// show results with fade in.
if (document.getElementById("submitted").innerHTML === "true") {
    fadeIn(fadeInDiv);
}

// fade functions for help.
function fadeInHelp() {
    if (helpDiv.style.opacity < 0.1) {
        document.getElementById("help").innerHTML = "Hide help <button class=\"spoilerbutton\" " +
            "onclick=\"fadeOutHelp()\">-</button>";
        fadeIn(helpDiv);
    }
}

function fadeOutHelp() {
    if (helpDiv.style.opacity >= 1.0) {
        document.getElementById("help").innerHTML = "Show help <button class=\"spoilerbutton\" onclick=\"fadeInHelp()" +
            "\">+</button>";
        fadeOut(helpDiv);
    }

}

function fadeIn(element) {
    var op = 0.1;  // initial opacity
    element.style.display = 'block';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}

function fadeOut(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 10);
}