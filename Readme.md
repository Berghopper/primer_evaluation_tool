### README ###
#### Primer Evaluation Web Tool - Casper Peters (Berghopper) ####

This is my repo for the final assignment "Primer Evaluation Tool", only raw code will be submitted here along with gradle files to build a war. 

This war can then be deployed under tomcat to use the webtool.

#### What is this tool? ####
This tool is meant for analysing DNA primer(s) in multiple ways:

* GC percentage
* melting point
* the maximum homopolymer stretch between 2 primers
* the maximum intermolecular identity between 2 primers
* the maximum intramolecular identity of the primer(s)

A homopolymer stretch is a repition of 1 base. An intermolecular identity is a complement match between the 2 primers, where one of the primers is reverse stranded. An intramolecular identity is a complement match within the same primer on its own reverse strand.

At least one or both primers need to be input, the only legal DNA characters to use are "A", "C", "T", "G", "a", "c", "t" and "g".

